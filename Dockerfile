FROM debian:stretch-slim

RUN apt-get update && \
    apt-get install libgmp10 && \
    rm -rf /var/lib/apt/lists

COPY bin/hlint /usr/local/bin/hlint

ENTRYPOINT ["/usr/local/bin/hlint"]
